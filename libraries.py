class Library(object):
	"""docstring for Library"""
	def __init__(self):
		super(Library, self).__init__()
		self.clear = 1
		self.dictionary = {}

	def increase(self):
		self.clear += 1

	def decrease(self):
		self.clear -= 1